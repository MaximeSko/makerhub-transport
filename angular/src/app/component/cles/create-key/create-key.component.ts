import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LicenceDto } from 'src/app/dto/LicenceDto';
import{LicenceService} from '../../../services/licence.service';
import { NbToastrService } from '@nebular/theme';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-key',
  templateUrl: './create-key.component.html',
  styleUrls: ['./create-key.component.scss']
})
export class CreateKeyComponent implements OnInit {
mois:number;
  formDetail:FormGroup;
  Ladate:string;
  date:Date;
  spliter:string[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public Datea: Date,
    private LicenceService : LicenceService,
    private Toastr:NbToastrService,
    private dialogService:  MatDialogRef<CreateKeyComponent>,

  ) { }

  ngOnInit(): void {

    this.formDetail=new FormGroup({
      'date':new FormControl(null,Validators.compose([
        Validators.required
      ])),
    })
  }

changeDate(date){
  var theDate = new Date(Date.parse(date));
  const localDate = theDate.toLocaleString().split(",");

  this.spliter=localDate[0].split("/")
  console.log(this.spliter);
  this.Ladate=this.spliter[2]+"-"+this.spliter[1]+"-"+this.spliter[0];
  console.log(this.Ladate);
}

send(){
   try{
      this.LicenceService.create(this.Ladate);
        this.LicenceService.refresh();
        this.Toastr.primary("Licence Crée");
        this.dialogService.close();

  }
  catch{
     this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
   }
 }
}






import { Component, Inject, inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { NbToastrService } from '@nebular/theme';
import { LicenceDto } from 'src/app/dto/LicenceDto';
import{LicenceService} from '../../../services/licence.service'
import { ClesComponent } from '../cles.component';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent  implements OnInit {
  formDetail:FormGroup;
  licence:LicenceDto;
  valide:string;


  constructor(@Inject(MAT_DIALOG_DATA) public id: any,
    private LicenceService : LicenceService,
    private Toastr:NbToastrService,
    private dialogService:  MatDialogRef<DetailComponent>,) { }

  ngOnInit(): void {
    this.dialogService.keydownEvents().subscribe(event => {
      if (event.key === "Escape") {
        this.dialogService.close();

      }
  });
  console.log(this.id.id);
  this.LicenceService.getDetails(this.id.id).subscribe(data =>{
    this.licence=data;
    console.log(data.isValide);
    if(this.licence.isValide){
      this.valide="Active";
    }
    else{
      this.valide="desactivée";
    }
    // this.formDetail.patchValue({"id":, "DateCreation":,"Validite":this.Licence.valide,"dateDeValidite":this.licence.dateDeValidite});
    this.formDetail=new FormGroup({
      'id':new FormControl(this.licence.id,Validators.compose([
      ])),
      'DateCreation':new FormControl(this.licence.dateCreation,Validators.compose([
        Validators.required
      ])),
      'isValide':new FormControl(this.licence.isValide,Validators.compose([
        Validators.required
      ])),
      'dateDeValidite':new FormControl(this.licence.dateDeValidite,Validators.compose([
        Validators.required
      ])),
    })
    // this.formDetail.disable();

  })



   }



  Delete(){
    try{
      // console.log(this.id.id);
      this.LicenceService.delete(this.id.id).subscribe(x => {
        this.LicenceService.refresh();
        this.Toastr.primary("Licence supprimée");
        this.dialogService.close();
      })
    }
    catch{
      this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
    }
  }



  enregistrer(){
    try{

      console.log(this.formDetail.value);

      this.LicenceService.update(this.formDetail.value).subscribe(data => {
        this.LicenceService.refresh();
        this.Toastr.primary("Licence mise a jour");
        this.dialogService.close();
      })
    }
    catch{
      this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
    }
  }



}

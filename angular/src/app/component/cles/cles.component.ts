import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LicenceDto } from 'src/app/dto/LicenceDto';
import {LicenceService} from '../../services/licence.service'
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { DetailComponent } from './detail/detail.component';
import { PageEvent } from '@angular/material/paginator';
import { ThrowStmt } from '@angular/compiler';
import { CreateKeyComponent } from './create-key/create-key.component';

@Component({
  selector: 'app-cles',
  templateUrl: './cles.component.html',
  styleUrls: ['./cles.component.scss']
})
export class ClesComponent implements OnInit {
  total : number = 0;
  displayedColumns: string[] = ['Id', 'Date-de-Creation','Validitée','DateDeValidite'];
  dataArray : LicenceDto[]=[];

  dataSource = new MatTableDataSource(this.dataArray);
  constructor(private LicenceService : LicenceService,private Toastr:NbToastrService,private _location:Location , private dialog: MatDialog) { }

  ngOnInit(): void {
    this.LicenceService.getMax().subscribe(d => {
      this.total=d
    })

    this.LicenceService.get(0,10).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
    },error =>{
      this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
    });
  }
  onPage(pageEvent: PageEvent) {
    console.log(pageEvent)
    this.LicenceService.get((pageEvent.pageIndex) * 10, 10).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
      // console.log(this.dataArray);
    })
  }
  

  return(){
    this._location.back();
  }
  openDetail(id : string){
    console.log(id)
    let ref= this.dialog.open(DetailComponent, { disableClose: true,data:{id}, panelClass: 'custom-dialog-container' });
    ref.afterClosed().subscribe(()=>{
      this.LicenceService.get(0,10).subscribe(data => {
        this.dataArray=data;
        this.dataSource = new MatTableDataSource(this.dataArray);
      },error =>{
        this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
      })
    });
    // console.log("ouverture de la fenetre")
  }

  public doFilter = (value: string) => {
    this.LicenceService.find(value).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
    });
  }


  addKey(){
    let ref= this.dialog.open(CreateKeyComponent, { disableClose: true, panelClass: 'custom-dialog-container' });
    ref.afterClosed().subscribe(()=>{
      this.LicenceService.get(0,10).subscribe(data => {
        this.dataArray=data;
        this.dataSource = new MatTableDataSource(this.dataArray);
      })
    });
  }


}

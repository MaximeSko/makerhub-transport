import { Component, OnInit, Input, Inject, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { ClientServicesService } from '../../../services/Client-services.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ClientDto } from '../../../dto/ClientDto';
import { AddKeyToClientComponent } from '../../cles/add-key-to-client/add-key-to-client.component';
import { LicenceDto } from 'src/app/dto/LicenceDto';
import { ITS_JUST_ANGULAR } from '@angular/core/src/r3_symbols';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-get-client-key',
  templateUrl: './get-client-key.component.html',
  styleUrls: ['./get-client-key.component.scss']
})
export class GetClientKeyComponent implements OnInit {
  formDetail:FormGroup;
  Client :ClientDto;
  Licences:Array<LicenceDto>;
  dataArray :Array<LicenceDto>=[];
  displayedColumns: string[] = ['Id','btn'];
  dataSource = new MatTableDataSource(this.dataArray);

  constructor(private dialogService:  MatDialogRef<GetClientKeyComponent>,private dialog: MatDialog,private changeDection : ChangeDetectorRef,private Toastr:NbToastrService,private ClientServices : ClientServicesService,
    @Inject(MAT_DIALOG_DATA) public id: any) { }

  ngOnInit(): void {
    this.dialogService.keydownEvents().subscribe(event => {
      if (event.key === "Escape") {
        this.dialogService.close();
      }
  });
    //  console.log(this.id.id);
    this.ClientServices.getDetails(this.id.id).subscribe(data =>{


      // console.log(data);
      // console.log(data.licences[0]);
      this.Client=data;
      this.dataArray=data.licences;
      this.dataSource = new MatTableDataSource(this.dataArray);
    })
    this.formDetail = new FormGroup({
      LicenceKey: new FormControl()


   });


  }

  Delete(id:string){
    try{

      console.log("delete");
      console.log(this.formDetail.parent);
      console.log(this.Client.id);

     this.ClientServices.DelLicence({licenceId:id,clientId:this.Client.id}).subscribe(data=>{
        this.Toastr.success('Utilisateur Modifié');
     });
      this.dialog.closeAll();
    }
    catch{
      this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
    }
  }

  openAddKey(){
      this.dialogService.close();
      this.dialog.open(AddKeyToClientComponent, { disableClose: true,data:{id:this.id.id}, panelClass: 'cust-dialog-containerTwo' });
    console.log("ouverture de la fenetre")


  }
}

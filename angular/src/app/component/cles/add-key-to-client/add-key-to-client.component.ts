import { Component, Inject, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LicenceDto } from 'src/app/dto/LicenceDto';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ThrowStmt } from '@angular/compiler';
import {LicenceService}from '../../../services/licence.service'
import {ClientServicesService} from '../../../services/Client-services.service'
import { CreateKeyComponent } from '../create-key/create-key.component';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-add-key-to-client',
  templateUrl: './add-key-to-client.component.html',
  styleUrls: ['./add-key-to-client.component.scss']
})
export class AddKeyToClientComponent implements OnInit {

  total : number = 0;
  displayedColumns: string[] = ['Id', 'Date-de-Creation', 'Validitée','btn',"btnTwo"];
  dataArray : LicenceDto[]=[];
  dataSource = new MatTableDataSource(this.dataArray);
  constructor(private LicenceService : LicenceService,private ClientServices : ClientServicesService,private Toastr:NbToastrService,private _location:Location , private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public ClientId: any) { }

  ngOnInit(): void {

    this.LicenceService.getMax().subscribe(d => {
      this.total=d
    })
    this.LicenceService.get(0,10).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
    },error =>{
      this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
    });
  }
  onPage(pageEvent: PageEvent) {
    console.log(pageEvent)
    this.LicenceService.get((pageEvent.pageIndex) * 10, 10).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
      // console.log(this.dataArray);
    })
  }


  return(){
    this._location.back();
  }
  openDetail(id : string){
    console.log(id)
    let ref= this.dialog.open(DetailComponent, { disableClose: true,data:{id}, panelClass: 'custom-dialog-container' });
    ref.afterClosed().subscribe(()=>{
      this.LicenceService.get(0,10).subscribe(data => {
        this.dataArray=data;
        this.dataSource = new MatTableDataSource(this.dataArray);
      },error =>{
        this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
      })
    });
    // console.log("ouverture de la fenetre")
  }

  public doFilter = (value: string) => {
    this.LicenceService.find(value).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
    });
  }


  addKey(){
    let ref= this.dialog.open(CreateKeyComponent, { disableClose: true, panelClass: 'custom-dialog-container' });
    ref.afterClosed().subscribe(()=>{
      this.LicenceService.get(0,10).subscribe(data => {
        this.dataArray=data;
        this.dataSource = new MatTableDataSource(this.dataArray);
      })
    });
  }
  select(Licenceid:string){
    console.log(Licenceid);
   console.log("open select"+this.ClientId.id);
   this.ClientServices.AddLicence({licenceId:Licenceid,clientId:this.ClientId.id}).subscribe(data=>{
      this.Toastr.success('Utilisateur Modifié');
   });
    this.dialog.closeAll();

 // }
  }

}

import { Component, OnInit } from '@angular/core';
import { NbDialogRef, NbDialogService, NbToastrService } from '@nebular/theme';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import {ClientServicesService} from '../../services/Client-services.service'
import { NgForOf } from '@angular/common';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-compte-client-edition',
  templateUrl: './compte-client-edition.component.html',
  styleUrls: ['./compte-client-edition.component.scss']
})


export class CompteClientEditionComponent implements OnInit {
  form:FormGroup;
  constructor(
    private Toastr:NbToastrService,
    private ClientServices : ClientServicesService,
    private dialogService:  MatDialogRef<CompteClientEditionComponent>,) { } 

  ngOnInit(): void {

    
    this.form=new FormGroup({
      'AdresseMail':new FormControl(null,Validators.compose([
        Validators.email,
        Validators.required
      ])),
      'Nom':new FormControl(null,Validators.compose([
        Validators.pattern(/^[a-z]+(?:['_.-\s][a-z]+)*$/i),
        Validators.required
      ])),
      'Prenom':new FormControl(null,Validators.compose([
        Validators.pattern(/^[a-z]+(?:['_.-\s][a-z]+)*$/i),
        Validators.required
      ])),
      'NomSociete':new FormControl(null,Validators.compose([
        Validators.required
      ])),
    });

    this.dialogService.keydownEvents().subscribe(event => {
      if (event.key === "Escape") {
        this.dialogService.close();
      }
  });
  }
  
  onFormSubmit(formDirective: FormGroupDirective){
    // console.log(this.form.value)
    if(this.form.valid){

      this.ClientServices.Save(this.form.value).subscribe(data=>{
        this.Toastr.success('Utilisateur Crée')
        this.form.reset();
        formDirective.resetForm();
        this.ClientServices.refresh();
        this.dialogService.close();
      },error => {
        this.Toastr.danger(error.error)
      })
    }
    else{
      this.Toastr.danger('Erreur veuillez completer le formulaire correctement')
    }
  }
  
}

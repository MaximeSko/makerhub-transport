import { Component, OnInit, Input, Inject, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { ClientServicesService } from '../../../services/Client-services.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ClientDto } from '../../../dto/ClientDto';
import { ClientDeleteComponent } from '../client-delete/client-delete.component';
import { AddKeyToClientComponent } from '../../cles/add-key-to-client/add-key-to-client.component';
import { GetClientKeyComponent } from '../../cles/get-client-key/get-client-key.component';

@Component({
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.scss']
})
export class ClientDetailComponent implements OnInit {
  formDetail:FormGroup;
  Client :ClientDto;
  forulaireSatus:boolean;
  key:boolean;

  constructor(private dialogService:  MatDialogRef<ClientDetailComponent>,private dialog: MatDialog,private changeDection : ChangeDetectorRef,private Toastr:NbToastrService,private ClientServices : ClientServicesService,
    @Inject(MAT_DIALOG_DATA) public id: any
    ){ }

  ngOnInit(): void{
    this.dialogService.keydownEvents().subscribe(event => {
      if (event.key === "Escape") {
        this.dialogService.close();
      }
  });
     console.log(this.id.id);
    this.ClientServices.getDetails(this.id.id).subscribe(data =>{
      this.Client=data;
      if(data.licences.length!=0){
        this.key=true;
      }
      else{
        this.key=false;
      }
      this.formDetail.patchValue({"AdresseMail":this.Client.adresseMail, "Nom":this.Client.nom,"Prenom":this.Client.prenom,"NomSociete":this.Client.nomsociete});
      this.formDetail.disable();
      this.forulaireSatus=true;
      // console.log(this.formDetail.status)
    })

    this.formDetail=new FormGroup({
      'AdresseMail':new FormControl(null,Validators.compose([
        Validators.email,
        Validators.required
      ])),
      'Nom':new FormControl(null,Validators.compose([
        Validators.pattern(/^[a-z]+(?:['_.-\s][a-z]+)*$/i),
        Validators.required
      ])),
      'Prenom':new FormControl(null,Validators.compose([
        Validators.pattern(/^[a-z]+(?:['_.-\s][a-z]+)*$/i),
        Validators.required
      ])),
      'NomSociete':new FormControl(null,Validators.compose([
        Validators.required
      ])),
      'Id':new FormControl(this.id.id,Validators.compose([
      ])),
      'licences':new FormControl(this.id.licences,Validators.compose([
      ])),
    });
  }

  onFormSubmit(formDirective: FormGroupDirective){
    console.log(this.formDetail.value)
    if(this.formDetail.valid){

      this.ClientServices.update(this.formDetail.value).subscribe(data=>{
        this.ClientServices.refresh();
        this.Toastr.success('Utilisateur Modifié');
        this.dialogService.close();
      },error => {
        this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
      })
    }
    else{
      this.Toastr.danger('Erreur veuillez completer le formulaire correctement')
    }
  }
  send(){
    console.log(this.formDetail.value)
    if(this.formDetail.valid){

      this.ClientServices.update(this.formDetail.value).subscribe(data=>{
        this.ClientServices.refresh();
        this.Toastr.success('Utilisateur Modifié');
        this.dialogService.close();
      },error => {
        this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
      })
    }
    else{
      this.Toastr.danger('Erreur veuillez completer le formulaire correctement')
    }
  }


  enableButton(){
    this.formDetail.enable();
    this.forulaireSatus=false;
    this.changeDection.detectChanges();


  }

  openDelete(id : number){
    console.log(id)

      this.dialog.open(ClientDeleteComponent, { disableClose: true,data:{id:id}, panelClass: 'custom-dialog-container' });
      this.dialogService.close();
      this.dialogService.afterClosed().subscribe(result => {
        this.ClientServices.refresh();
      });
     console.log("ouverture de la fenetre")
  }

  openAddKey(id : number){

    console.log(id)
    if(this.key == true){
      this.dialogService.close();
      this.dialog.open(GetClientKeyComponent, { disableClose: true,data:{id:id}, panelClass: 'cust-dialog-containerTwo' });
    console.log("ouverture de la fenetre")
    }
    else{
      this.dialogService.close();
      this.dialog.open(AddKeyToClientComponent, { disableClose: true,data:{id:id}, panelClass: 'cust-dialog-containerTwo' });
    console.log("ouverture de la fenetre")
    }

  }

}

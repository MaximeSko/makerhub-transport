import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CompteClientEditionComponent } from '../compte-client-edition/compte-client-edition.component';
import { NbDialogService } from '@nebular/theme';
import{MatDialog, MatDialogConfig} from '@angular/material/dialog'
import { ClientDto } from '../../dto/ClientDto';
import {ClientServicesService} from '../../services/Client-services.service'
import { MatTableDataSource } from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {ClientDetailComponent} from '../compte-client-gestion/client-detail/client-detail.component'
import { ClientDeleteComponent } from './client-delete/client-delete.component';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { RouterLink } from '@angular/router';
import { ClesComponent } from '../cles/cles.component';


@Component({
  selector: 'app-compte-client-gestion',
  templateUrl: './compte-client-gestion.component.html',
  styleUrls: ['./compte-client-gestion.component.scss']

})
export class CompteClientGestionComponent implements OnInit,AfterViewInit {
  total : number = 0;
  dataArray :ClientDto[]=[];
  displayedColumns: string[] = ['Id', 'Email', 'Nom', 'Prenom','Nom de Société'];

  dataSource = new MatTableDataSource(this.dataArray);
  constructor(private dialog: MatDialog,
    private ClientServices : ClientServicesService,
    ){}
     @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {

    this.ClientServices.getMax().subscribe(d => {
      this.total=d
    })
    this.ClientServices.get(0, 10).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
      // this.dataSource.sort = this.sort;
      // console.log(this.dataArray);
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

   open(){

     let ref = this.dialog.open(CompteClientEditionComponent, { disableClose: true, panelClass: 'custom-dialog-container'});
     ref.afterClosed().subscribe(()=>{
      this.ClientServices.get(0, 10).subscribe(data => {
        this.dataArray=data;
        this.dataSource = new MatTableDataSource(this.dataArray);
        // this.dataSource.sort = this.sort;
        // console.log(this.dataArray);
      });
     });

   }



  onPage(pageEvent: PageEvent) {
    console.log(pageEvent)
    this.ClientServices.get((pageEvent.pageIndex) * 10, 10).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
      // console.log(this.dataArray);
    })
  }

  openDetail(id : number){
    console.log(id)
     let dialogRef = this.dialog.open(ClientDetailComponent, { disableClose: true,data:{id:id}, panelClass: 'custom-dialog-container' });
     dialogRef.afterClosed().subscribe(() =>{
       console.log("afterclosed");
      this.ClientServices.get(0, 10).subscribe(data => {
        this.dataArray=data;
        this.dataSource = new MatTableDataSource(this.dataArray);
      });
     });
    // console.log("ouverture de la fenetre")
  }

  public doFilter = (value: string) => {
    this.ClientServices.find(value).subscribe(data => {
      this.dataArray=data;
      this.dataSource = new MatTableDataSource(this.dataArray);
    });
  }

}

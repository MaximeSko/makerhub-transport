import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { ClientServicesService } from '../../../services/Client-services.service';
import { ClientDto } from '../../../dto/ClientDto';

@Component({
  templateUrl: './client-delete.component.html',
  styleUrls: ['./client-delete.component.scss']
})

export class ClientDeleteComponent implements OnInit {
  
  dataArray :ClientDto[]=[];
  constructor(@Inject(MAT_DIALOG_DATA) public id: any,
  private dialogService:  MatDialogRef<ClientDeleteComponent>,
  private Toastr:NbToastrService,
  private ClientServices : ClientServicesService,
  private changeDection : ChangeDetectorRef
  ) {}

   Client :ClientDto;

  ngOnInit(): void {
    this.dialogService.keydownEvents().subscribe(event => {
      if (event.key === "Escape") {
        this.dialogService.close();
        
      }
  });
    // console.log(this.id.id);
    this.ClientServices.getDetails(this.id.id).subscribe(data => {
      this.Client=data;
      this.dataArray.push(this.Client);
      console.log(this.dataArray);
      this.changeDection.detectChanges();
    })
  }

  Supprimer(){
    try{
      // console.log(this.id.id);
      this.ClientServices.delete(this.id.id).subscribe(x => {
        this.ClientServices.refresh();
        this.Toastr.primary("Utilisateur Supprimé");
        this.dialogService.close();
      })
    }
    catch{
      this.Toastr.primary("Une erreur inattendue, veuillez contacter votre technicien")
    } 
  }
}

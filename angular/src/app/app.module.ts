import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NbThemeModule,NbLayoutModule,NbCardModule, NbButtonModule, NbMenuModule, NbIconModule, NbDialogModule, NbWindowModule, NbInputModule, NbToastrModule} from '@nebular/theme'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompteClientGestionComponent } from './component/compte-client-gestion/compte-client-gestion.component';
import { CompteClientEditionComponent } from './component/compte-client-edition/compte-client-edition.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { from } from 'rxjs';
import { config } from 'process';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputColorPipe } from './pipes/input-color.pipe';
import { HttpClientModule } from '@angular/common/http';

import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import { ClientDetailComponent } from './component/compte-client-gestion/client-detail/client-detail.component';
import { ClientDeleteComponent } from './component/compte-client-gestion/client-delete/client-delete.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { ClesComponent } from './component/cles/cles.component';
import { RouterModule } from '@angular/router';
import { DetailComponent } from './component/cles/detail/detail.component';
import { AddKeyToClientComponent } from './component/cles/add-key-to-client/add-key-to-client.component';
import { GetClientKeyComponent } from './component/cles/get-client-key/get-client-key.component';
import {MatDatepickerModule} from '@angular/material/datepicker'
import { MatNativeDateModule } from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import { CreateKeyComponent } from './component/cles/create-key/create-key.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';



@NgModule({
  declarations: [
    AppComponent,
    CompteClientGestionComponent,
    CompteClientEditionComponent,
    InputColorPipe,
    ClientDetailComponent,
    ClientDeleteComponent,
    ClesComponent,
    DetailComponent,
    AddKeyToClientComponent,
    GetClientKeyComponent,
    CreateKeyComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'corporate' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbMenuModule.forRoot(),
    NbDialogModule.forRoot(),
    NbInputModule,
    NbToastrModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NbEvaIconsModule,
    HttpClientModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatChipsModule,
    MatFormFieldModule,
    MatSelectModule


  ],
  providers: [
    MatDatepickerModule,
    MatNativeDateModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


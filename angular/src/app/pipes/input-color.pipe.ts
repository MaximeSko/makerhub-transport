import { Pipe, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';

@Pipe({
  name: 'inputColor',
  pure: false
})
export class InputColorPipe implements PipeTransform {

  transform(value: FormControl, ...args: unknown[]): string {
    if(value.untouched)
      return 'info';
    else if(value.invalid){
      return 'danger';
    }
    return 'success';
  }

}

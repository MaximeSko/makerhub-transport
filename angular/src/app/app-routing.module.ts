import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CompteClientGestionComponent} from '../app/component/compte-client-gestion/compte-client-gestion.component'
import { ClesComponent } from './component/cles/cles.component';

const routes: Routes = [
  { path:'',component:CompteClientGestionComponent } ,
  {path:'cles',component:ClesComponent,} ,
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export interface LicenceDto{
    id : string;
    dateCreation:Date;
    isValide:boolean;
    dateDeValidite:Date;
}

import { LicenceDto } from './LicenceDto';

export interface ClientDto{
    id :number;
    adresseMail : string;
    nom : string;
    prenom : string;
    nomsociete: string;
    licences:LicenceDto[];
}
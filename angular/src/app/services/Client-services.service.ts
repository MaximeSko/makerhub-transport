import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ClientDto } from '../dto/ClientDto';
import { HttpClient, HttpParams } from '@angular/common/http'
 import {LicenceToClientDto} from '../dto/LicenceToCLientDTO'

@Injectable({
  providedIn: 'root'
})
export class ClientServicesService {

  context:BehaviorSubject<ClientDto[]>;
  private readonly Key_Api : string = "http://localhost/MKH_Api/Client";

  constructor(private client:HttpClient) {
     this.context= new BehaviorSubject<ClientDto[]>(null);
     this.refresh();
  }
  refresh(){
    console.log("refrech1");
    this.client.get<ClientDto[]>(this.Key_Api).subscribe(data => {
      console.log("refrech2");
      return this.context.next(data);

    })
  }
  get(offset : number, limit : number){
    console.log("refrech3")
    return this.client.get<ClientDto[]>(this.Key_Api+"?offset="+offset + "&limit=" + limit)
  }
  Save(Client : ClientDto) : Observable<ClientDto>{
    return this.client.post<ClientDto>(this.Key_Api,Client)
  }
  getDetails(id : number){
    return this.client.get<ClientDto>(this.Key_Api+"/"+id)
  }
  delete(id :number){
    console.log("Deleted", id);
    return this.client.delete<ClientDto>(this.Key_Api+"/"+id)
  }
  update(Client : ClientDto){
    return this.client.put<ClientDto>(this.Key_Api,Client)
  }
  getMax(){
    return this.client.get<number>(this.Key_Api+"/total")
  }
   AddLicence(LicenceToCLient:LicenceToClientDto){
     return this.client.post<LicenceToClientDto>(this.Key_Api+"/AddLicence",LicenceToCLient)
   }
   DelLicence(LicenceToCLient:LicenceToClientDto){
    return this.client.post<LicenceToClientDto>(this.Key_Api+"/DeleteLicence",LicenceToCLient)
   }
   find(aChercher:string){
    return this.client.get<ClientDto[]>(this.Key_Api+"/find"+aChercher)
   }
}

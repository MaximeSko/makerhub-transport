import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {LicenceDto} from '../dto/LicenceDto'
import { HttpClient } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class LicenceService {

  context:BehaviorSubject<LicenceDto[]>;

  private readonly Key_Api : string = "http://localhost/MKH_Api/Licence";

  constructor(private licence:HttpClient) {
    this.context= new BehaviorSubject<LicenceDto[]>(null);
   }
   refresh(){
    this.licence.get<LicenceDto[]>(this.Key_Api).subscribe(data => {
      this.context.next(data)
      console.log(data)

    })
   }

  //  offset : number, limit : number
   get(offset : number, limit : number){
    // +"?offset="+offset + "&limit=" + limit
    return this.licence.get<LicenceDto[]>(this.Key_Api +"?offset="+offset + "&limit=" + limit)
  }

  delete(id :number){
    console.log("Deleted", id);
    return this.licence.delete<LicenceDto>(this.Key_Api+"/"+id)
  }

   create(date:string){
    this.licence.get<LicenceDto[]>(this.Key_Api+"/Create"+date).subscribe(data => {
      this.context.next(data);
    })
   }

   getMax(){
    return this.licence.get<number>(this.Key_Api+"/total")
  }

  update(Client : LicenceDto){
    return this.licence.put<LicenceDto>(this.Key_Api,Client)
  }

  getDetails(id : number){
    return this.licence.get<LicenceDto>(this.Key_Api+"/"+id)
  }

  find(aChercher:string){
    return this.licence.get<LicenceDto[]>(this.Key_Api+"/find"+aChercher)
   }
}




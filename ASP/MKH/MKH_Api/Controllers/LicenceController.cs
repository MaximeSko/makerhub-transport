﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MKH_Api.DTO;
using MKH_Api.Entity;
using MKH_Api.Helper;
using MKH_Api.Mapper;
using MKH_Api.Models;

namespace MKH_Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LicenceController : Controller
    {
        private readonly ApplicationContext _context;
        public LicenceController(ApplicationContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<List<LicenceDTO>> Get([FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            try
            {
                return _context.Licence.Skip(offset).Take(limit).ToList().Select(u => new LicenceDTO()
                {

                    Id = u.Id,
                    DateCreation = u.DateCreation,
                    IsValide = u.IsValide,
                    DateDeValidite=u.DateDeValidite,
                    ClientEntityId = u.ClientEntityId
                    
                }).ToList();
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
        [HttpGet]
        [Route("total")]
        public int Get()
        {
            {
                return _context.Licence.Count();
            }
        }
        [HttpGet]
        [Route("Create{monthsAdd}")]
        public IActionResult Create(DateTime monthsAdd)
        {

            try
            {
               
                
                {
                

                    LicenceDTO licence = new LicenceDTO
                    {
                        Id = Guid.NewGuid(),
                        DateCreation = DateTime.Now,
                        IsValide = true,
                        DateDeValidite = monthsAdd,
                        ClientEntityId = null,
                    };
                    _context.Add(licence.MappingL());
                    _context.SaveChanges();
                    return Ok(licence);
                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
        [HttpGet]
        [Route("{id}")]
        public ActionResult<LicenceDTO> Get(Guid id)
        {
            try
            {
                LicenceEntity u = _context.Licence.FirstOrDefault(u => u.Id == id);
                if (u != null)
                {
                    return new LicenceDTO()
                    {
                        Id = u.Id,
                        DateCreation = u.DateCreation,
                        DateDeValidite = u.DateDeValidite,
                        IsValide = u.IsValide
                        
                    };
                }
                else
                {
                    return BadRequest("La licence n'existe pas");
                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                using ApplicationContext db = new ApplicationContext();
                {
                    LicenceEntity clientDB = _context.Licence.FirstOrDefault(u => u.Id == id);
                    if (clientDB != null)
                    {
                        db.Remove(clientDB);
                        db.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("La Licence n'existe pas");
                    }
                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
        [HttpPut]
        public IActionResult Put(LicenceDTO client)
        {
            try
            {
                
                {
                    

                    LicenceEntity entity = client.MappingL();
                    _context.Update(entity);
                    //_context.Licence.Attach(entity);
                    //_context.Entry(entity).State = EntityState.Modified;


                    _context.SaveChanges();

                    return Ok();
                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("find{fi}")]
        public ActionResult<List<LicenceDTO>> find(string fi)
        {

            try
            {

                List<LicenceDTO> fusionList = _context.Licence.Where(c => c.Id.ToString().Contains(fi) || c.DateCreation.ToString().Contains(fi)).Select(u => new LicenceDTO()
                {
                    Id = u.Id,
                    DateCreation=u.DateCreation,
                    DateDeValidite = u.DateDeValidite,
                    IsValide = u.IsValide,
                   
                }).ToList();

             

                return fusionList;

            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


    }
}

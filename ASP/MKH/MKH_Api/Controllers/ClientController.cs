﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MKH_Api.Models;
using MKH_Api.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using MKH_Api.Mapper;
using System.Reflection;
using System.IO;
using MKH_Api.Helper;
using MKH_Api.DTO;
using System.Data.Entity.SqlServer;
using System.Threading;

namespace MKH_Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {


        #region Context
        // le context
        private readonly ApplicationContext _context;
        public ClientController(ApplicationContext context)
        {
            _context = context;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        /// <response code="200">La sauvegarde s'est bien passé</response>
        /// <response code="500">Une erreur inattendue est survenue</response>
        /// <response code="400">L'adresse mail existe déjà</response>

        [HttpGet]
        public ActionResult<List<ClientDTO>> Get([FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {

            try
            {
                return _context.Client.Skip(offset).Take(limit).ToList().Select(u => new ClientDTO()
                {
                    Id = u.Id,
                    AdresseMail = u.AdresseMail,
                    Nom = u.Nom,
                    Prenom = u.Prenom,
                    Nomsociete = u.Nomsociete,
                    Licences = _context.Licence.Where(l => l.ClientEntityId == u.Id).ToList(),
                }).ToList();
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


        [HttpGet]
        [Route("find{fi}")]
        public ActionResult<List<ClientDTO>> find(string fi)
        {
            
            try
            {
                List<ClientDTO> fusionList = _context.Client.Where(c => c.AdresseMail.Contains(fi) || c.Prenom.Contains(fi) || c.Nom.Contains(fi) || c.Nomsociete.Contains(fi)).Select(u => new ClientDTO()
                {
                    Id = u.Id,
                    AdresseMail = u.AdresseMail,
                    Nom = u.Nom,
                    Prenom = u.Prenom,
                    Nomsociete = u.Nomsociete,
                    Licences = _context.Licence.Where(l => l.ClientEntityId == u.Id).ToList(),
                }).ToList();



                return fusionList;
               
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("total")]
        public int Get()
        {
            {
    
                return _context.Client.Count();
            }
        }
        [HttpGet]
        [Route("{id}")]
        public ActionResult<ClientDTO> Get(int id)
        {
            try
            {
                ClientEntity u = _context.Client.FirstOrDefault(u => u.Id == id);
                if (u != null)
                {
                    return new ClientDTO()
                    {
                        Id = u.Id,
                        AdresseMail = u.AdresseMail,
                        Nom = u.Nom,
                        Prenom = u.Prenom,
                        Nomsociete = u.Nomsociete,
                        Licences = _context.Licence.Where(l => l.ClientEntityId == u.Id).ToList(),
                    };
                }
                else
                {
                    return BadRequest("Le client n'existe pas");
                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
        [HttpPost]
        public IActionResult Save(ClientDTO client)
        {
            try
            {
                {
                    ClientEntity EmailExist = _context.Client.FirstOrDefault(u => u.AdresseMail == client.AdresseMail);
                    if (EmailExist == null)
                    {
                        
                        _context.Add(client.Mapping());
                        _context.SaveChanges();
                    }
                    else return BadRequest("L'adresse mail existe déjà, l'adresse mail ne peut exister qu'une fois");

                    return Ok();
                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }
        [HttpPut]
        public IActionResult Put(ClientDTO client)
        {
            try
            {
                using (ApplicationContext db = new ApplicationContext())
                {

                    ClientEntity ClientUpd = _context.Client.FirstOrDefault(u => u.Id == client.Id);
                    if (ClientUpd.AdresseMail != client.AdresseMail)
                    {
                        ClientEntity EmailExist = _context.Client.FirstOrDefault(u => u.AdresseMail == client.AdresseMail);
                        if (EmailExist != null)
                        {
                            return BadRequest("L'adresse mail existe déjà, pour un autre client, l'adresse mail ne peut exister qu'une fois");
                        }
                    }

                    ClientEntity entity = client.Mapping();

                    db.Update(entity);
                    //_context.Client.Attach(entity);
                    //_context.Entry(entity).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok();
                }
      
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
                
        }

        [HttpPost]
        [Route("AddLicence")]
        public IActionResult Add(LicenceToClientDTO licenceTo)
        {
            try
            {
               
                {
                    LicenceEntity LicenceRecev = _context.Licence.FirstOrDefault(u => u.Id == licenceTo.LicenceId);
                    ClientEntity ClientUpd = _context.Client.FirstOrDefault(u => u.Id == licenceTo.ClientId);
                    if (LicenceRecev == null)
                    {
                        return BadRequest("La licence n'existe pas");
                    }
                    else if (ClientUpd == null)
                    {
                        return BadRequest("Le client n'existe pas");
                    }
                    else
                    {
                        LicenceRecev.ClientEntityId = ClientUpd.Id;
                        _context.Licence.Attach(LicenceRecev);
                        _context.Entry(LicenceRecev).State = EntityState.Modified;
                        _context.SaveChanges();
                        return Ok();
                    }

                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpPost]
        [Route("DeleteLicence")]
        public IActionResult Delete(LicenceToClientDTO licenceTo)
        {
            try
            {
                
                
                    LicenceEntity LicenceRecev = _context.Licence.FirstOrDefault(u => u.Id == licenceTo.LicenceId);
                    ClientEntity ClientUpd = _context.Client.FirstOrDefault(u => u.Id == licenceTo.ClientId);
                    if (LicenceRecev == null)
                    {
                        return BadRequest("La licence n'existe pas");
                    }
                    else if (ClientUpd == null)
                    {
                        return BadRequest("Le client n'existe pas");
                    }
                    else
                    {
                        LicenceRecev.ClientEntityId = null;
                        _context.Licence.Attach(LicenceRecev);
                        _context.Entry(LicenceRecev).State = EntityState.Modified;
                        _context.SaveChanges();
                        return Ok();
                    }

                
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
               
                    ClientEntity clientDB = _context.Client.FirstOrDefault(u => u.Id == id);
                    if (clientDB != null)
                    {

                    using (ApplicationContext db = new ApplicationContext())
                    {

                        List<LicenceDTO> Licences = _context.Licence.Where(l => l.ClientEntityId == id).Select(l => l.MappingE()).ToList();
                        foreach (LicenceDTO item in Licences)
                        {
                            item.ClientEntityId = null;
                            LicenceEntity entity = item.MappingL();
                            db.Update(entity);
                            db.SaveChanges();

                        }
                    }
                    using (ApplicationContext db = new ApplicationContext())
                    {

                        ClientEntity clientDBb = _context.Client.FirstOrDefault(u => u.Id == id);
                        db.Remove(clientDBb);
                        db.SaveChanges();
                        return Ok();
                    }
                    }
                    else
                    {
                        return BadRequest("Le client n'existe pas");
                    }
                
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

        [HttpGet]
        [Route("LicenceClient{id}")]
        public ActionResult<List<LicenceDTO>> GetLicenceClient(int id)
        {
            try
            {
                {
                    List<LicenceDTO> Licences = _context.Licence.Where(l => l.ClientEntityId == id).Select(l=>l.MappingE()).ToList();
                    return Licences;
                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
  
        }
    }
}

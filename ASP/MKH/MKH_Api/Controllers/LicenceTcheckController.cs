﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MKH_Api.DTO;
using MKH_Api.Entity;
using MKH_Api.Helper;
using MKH_Api.Mapper;
using MKH_Api.Models;

namespace MKH_Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LicenceTcheckController : Controller
    {
        private readonly ApplicationContext _context;
        public LicenceTcheckController(ApplicationContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<LicenceDTO> Get(Guid id)
        {
            try
            {
                LicenceEntity u = _context.Licence.FirstOrDefault(u => u.Id == id);
                if (u != null)
                {
                    if(u.DateDeValidite <= DateTime.Now && u.IsValide == true)
                    {

                        return Ok("Licence Valide");
                    }
                    return BadRequest("La licence n'est pas valide");

                }
                else
                {
                    return BadRequest("La licence n'existe pas");
                }
            }
            catch (Exception err)
            {
                LogHelper.LogError(err);
                return StatusCode(500, "Une erreur inattendue, veuillez contacter votre technicien");
            }
        }

    }
}

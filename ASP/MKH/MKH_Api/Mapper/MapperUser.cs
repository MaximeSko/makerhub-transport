﻿using MKH_Api.Entity;
using MKH_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MKH_Api.Mapper
{
    public static class MapperUser
    {
        public static ClientEntity Mapping(this ClientDTO dto)
        {
            return new ClientEntity()
            {
                Id = dto.Id,
                AdresseMail = dto.AdresseMail,
                Nom = dto.Nom,
                Prenom = dto.Prenom,
                Nomsociete = dto.Nomsociete,
                Licences=dto.Licences

            };
        }
    }
}

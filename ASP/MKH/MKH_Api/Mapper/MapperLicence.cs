﻿using MKH_Api.DTO;
using MKH_Api.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MKH_Api.Mapper
{
    public static class MapperLicence
    {
        public static LicenceEntity MappingL(this LicenceDTO dto)
        {
            return new LicenceEntity()
            {
                Id = dto.Id,
                DateCreation = dto.DateCreation,
                IsValide = dto.IsValide,
                ClientEntityId = dto.ClientEntityId,
                DateDeValidite = dto.DateDeValidite
                
                
            };
        }
        public static LicenceDTO MappingE(this LicenceEntity entity)
        {
            return new LicenceDTO()
            {
                Id = entity.Id,
                DateCreation = entity.DateCreation,
                IsValide = entity.IsValide,
                ClientEntityId = entity.ClientEntityId,
                DateDeValidite=entity.DateDeValidite

            };
        }

    }


}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MKH_Api.Helper
{
    public static class LogHelper
    {
        private static readonly string LogPath = "Log.txt";
        public static void LogError(Exception err, string user = null)
        {
            if (err == null) return;
            string u = user ?? Environment.UserName;

            if (LogPath != null) LogErrorInFile(Assembly.GetCallingAssembly(), err, u);
        }

        private static void LogErrorInFile(Assembly callingassembly, Exception err, string user)
        {
            WriteLog10Trial(callingassembly, err, Path.Combine(GetPathForLogInFile(callingassembly), "log.txt"), user);
        }

        private static bool WriteLog10Trial(Assembly assembly, Exception err, string pathLog, string user)
        {
            bool writeDone = false;
            int attempt = 0;
            while (!writeDone && attempt < 10)
            {
                writeDone = WriteLog(assembly, err, pathLog, user);
                attempt++;
            }
            return writeDone;
        }

        private static string GetPathForLogInFile(Assembly assembly)
        {
            string result = assembly != null ? assembly.Location : Assembly.GetExecutingAssembly().Location;
            if (LogPath != null)
            {
                if (!Directory.Exists(LogPath)) Directory.CreateDirectory(LogPath);
                result = LogPath;
            }
            return result;
        }

        private static bool WriteLog(Assembly assembly, Exception err, string pathLog, string user)
        {
            try
            {
                using (StreamWriter stream = new StreamWriter(pathLog, true))
                {
                    string assemblyName = assembly != null ? $" - { assembly.GetName()}:{ assembly.GetName().Version}" : string.Empty;

                    stream.WriteLine($"{DateTime.Now.ToString()}{assemblyName} - {user} - {err.GetType()} : {err.Message}");
                    Exception innerException = err?.InnerException;
                    stream.WriteLine(err.StackTrace?.ToString());
                    while (innerException != null)
                    {
                        stream.WriteLine($"InnerException - {innerException.GetType()} : {innerException?.Message}");
                        stream.WriteLine(innerException.StackTrace?.ToString());
                        innerException = innerException.InnerException;
                    }
                    stream.Close();
                    return true;
                }
            }
            catch (Exception) { return false; }
        }
    }
}

﻿using MKH_Api.DTO;
using MKH_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MKH_Api.Helper
{
    public static class TriList
    {
        public static List<ClientDTO> Tris(List<ClientDTO> ClientFusionList,List<ClientDTO> ClientList)
        {
            if (ClientList.Count != 0)
            {
                if (ClientFusionList.Count != 0)
                {

                    List<ClientDTO> tempList = new List<ClientDTO> { };

                    foreach (var prenomfinditem in ClientList)
                    {
                        bool dups = false;
                        foreach (var FusionListitem in ClientFusionList)
                        {
                            if (FusionListitem.Id == prenomfinditem.Id)
                            {
                                dups = true;
                            }
                        }
                        if (dups == false)
                        {
                            ClientFusionList.Add(prenomfinditem);
                        }
                    }
                }
                else
                {
                    foreach (var item in ClientList)
                    {

                        ClientFusionList.Add(item);
                    }
                }
            }
            return ClientFusionList;
        }

        public static List<LicenceDTO> TrisLicence(List<LicenceDTO> ClientFusionList, List<LicenceDTO> ClientList)
        {
            if (ClientList.Count != 0)
            {
                if (ClientFusionList.Count != 0)
                {

                    List<LicenceDTO> tempList = new List<LicenceDTO> { };

                    foreach (var prenomfinditem in ClientList)
                    {
                        bool dups = false;
                        foreach (var FusionListitem in ClientFusionList)
                        {
                            if (FusionListitem.Id == prenomfinditem.Id)
                            {
                                dups = true;
                            }
                        }
                        if (dups == false)
                        {
                            ClientFusionList.Add(prenomfinditem);
                        }
                    }
                }
                else
                {
                    foreach (var item in ClientList)
                    {

                        ClientFusionList.Add(item);
                    }
                }
            }
            return ClientFusionList;
        }


    }
}

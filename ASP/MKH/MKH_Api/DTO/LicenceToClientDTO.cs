﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MKH_Api.DTO
{
    public class LicenceToClientDTO
    {
        public Guid LicenceId { get; set; }
        public int ClientId { get; set; }
    }
}

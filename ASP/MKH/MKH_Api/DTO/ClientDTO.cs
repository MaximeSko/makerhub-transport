﻿using MKH_Api.DTO;
using MKH_Api.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MKH_Api.Models
{
    public class ClientDTO
    {
        public int Id { get; set; }
        public string AdresseMail { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Nomsociete { get; set; }
        public IEnumerable<LicenceEntity> Licences { get; set; }
        

    }
}

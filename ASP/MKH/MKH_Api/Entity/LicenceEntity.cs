﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MKH_Api.Entity
{
    public class LicenceEntity
    {
            public Guid Id { get; set; }
            public DateTime DateCreation { get; set; }
            public bool IsValide { get; set; }
             public DateTime DateDeValidite { get; set; }
             public int? ClientEntityId { get; set; }
    }
}

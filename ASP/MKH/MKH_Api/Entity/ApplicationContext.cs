﻿using Microsoft.EntityFrameworkCore;
using MKH_Api.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// context de l'application 

namespace MKH_Api.Entity
{
    public class ApplicationContext : Microsoft.EntityFrameworkCore.DbContext
    {
        //set les tables de la Db
        public DbSet<ClientEntity> Client { get; set; }
        public DbSet<LicenceEntity> Licence { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientEntity>()
                // unique UserName
                .HasIndex(p => new { p.Id })
                .IsUnique(true);

            modelBuilder.Entity<ClientEntity>()
                .HasMany(k => k.Licences);

            modelBuilder.Entity<LicenceEntity>()
                .Property(b => b.ClientEntityId).HasDefaultValue(null) ;
                //any to many
                //.HasOne(c => c.Client)
                //.WithMany(l => l.Licences);




        }

        // creation de la Db
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Server=TECHNOBEL ;Database=MKH;integrated security=SSPI
            // Server=5221\maxsqlserver;Database=MKH;user id=sa;password=test1234=;persist security info=True;MultipleActiveResultSets=true
            //Server=DESKTOP-7BS5DA4\SQLSERVER ;Database=MKH;integrated security=SSPI
            //Data Source=tcp:technobel.database.windows.net,1433;Initial Catalog=maximedb;User Id=maxime@technobel;Password=lK@452$lo
            optionsBuilder.UseSqlServer(@"Server=DESKTOP-QAGBT2F ;Database=MKH; uid=sa;pwd=test1234=;");
        }


    }
}
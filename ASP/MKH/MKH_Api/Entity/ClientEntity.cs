﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MKH_Api.Entity
{
    public class ClientEntity
    {
        public int Id { get; set; }
        public string AdresseMail { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Nomsociete { get; set; }
        public IEnumerable<LicenceEntity> Licences { get; set; }

    }
}
